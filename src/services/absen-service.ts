import {Injectable} from "@angular/core";
import {BaseAPI} from "./BaseAPI";
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable()
export class AbsenService {
    private absen: Array<DataSnapshot>;

    constructor(uid: string) {
        this.absen = [];
        BaseAPI.getFromDB('absen/' + uid)
            .then(data => {
                    this.absen = data.val();
                },
                error => console.log(error)
            );
    }

    getAll() {
        console.log(this.absen);
        return this.absen;
    }

    getItem(id) {
        return this.absen[id];
    }

    remove(item) {
        this.absen.splice(this.absen.indexOf(item), 1);
    }

}

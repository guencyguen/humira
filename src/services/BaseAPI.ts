import firebase from 'firebase';

export class BaseAPI {
    static getFromDB(path: string) {
        return firebase.database().ref("/Humira/" + path).once('value');
    }

    static setToDB(dest: string, data: any) {
        return firebase.database().ref("/Humira/" + dest).update(data);
    }
}

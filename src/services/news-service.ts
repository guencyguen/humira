import {Injectable} from "@angular/core";
import {BaseAPI} from "./BaseAPI";

@Injectable()
export class NewsService{
    private news: Array<Object>;

  constructor(){
      this.news = [];
      BaseAPI.getFromDB('news')
          .then(data => {
                  this.news = data.val();
                  console.log(this.news);
              },
              error => console.log(error)
          );
  }

  getAll(){
      return this.news;
  }
  
  getItem(id) {
      return this.news[id];
  }

  remove(item) {
    this.news.splice(this.news.indexOf(item), 1);
  }

}

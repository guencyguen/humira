import {Injectable} from "@angular/core";
import {BaseAPI} from "./BaseAPI";

@Injectable()
export class UserService {
    private users: Array<Object>;

    constructor() {
        this.users = [];
        BaseAPI.getFromDB('user')
            .then(data => {
                    this.users = data.val();
                    console.log(this.users);
                },
                error => console.log(error)
            );
    }

    getAll() {
        return this.users;
    }

    getItem(id) {
        return this.users[id];
    }

    remove(item) {
        this.users.splice(this.users.indexOf(item), 1);
    }
}

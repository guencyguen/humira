import { Injectable } from "@angular/core";
import{HISTORY} from "./mock-riwayatizin"

@Injectable()
export class RiwayatIzinService{
  private hist: any;

  constructor(){
    this.hist = HISTORY;
  }

  getAll(){
    return this.hist;
  }
  
  getItem(id) {
    for (var i = 0; i < this.hist.length; i++) {
      if (this.hist[i].id === parseInt(id)) {
        return this.hist[i];
      }
    }
    return null;
  }

  remove(item) {
    this.hist.splice(this.hist.indexOf(item), 1);
  }
}

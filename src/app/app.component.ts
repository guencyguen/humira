import {Component, ViewChild} from '@angular/core';
import {MenuController, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {LoginPage} from '../pages/login/login';
import {ProfilePage} from '../pages/profile/profile';
// import { EditprofilePage } from '../pages/edit-profile/edit-profile'
import {SettingsPage} from '../pages/settings/settings';
import {PermissionHistoryPage} from '../pages/permission-history/permission-history';
import {PresenceHistoryPage} from '../pages/presence-history/presence-history';
import {AuthService} from '../services/auth.service';
import {PermissionRequestPage} from '../pages/permission-request/permission-request';
import {NewsHistoryPage} from '../pages/news-history/news-history';
import {AdminEmployeesListPage} from '../pages/admin-employees-list/admin-employees-list';
import {AdminEmployeesAddPage} from '../pages/admin-employees-add/admin-employees-add';
import {AdminPermissionListPage} from '../pages/admin-permission-list/admin-permission-list';
import {AdminNewsAddPage} from '../pages/admin-news-add/admin-news-add';

@Component({
  templateUrl: 'app.html'
})
export class MyApp
{
	@ViewChild(Nav) nav: Nav;

	rootPage: any = LoginPage;
	pages: Array<{title: string, component: any}>;
	menu: MenuController;
    isCompany: boolean = false;
    user: any;
    rootParam: any;

	constructor(
        private auth: AuthService,
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        menu: MenuController
		) {
		this.initializeApp();
		this.menu = menu;
		this.pages = [
			{ title: 'Home', component: HomePage },
			{ title: 'List', component: ListPage }
		];

	}

    initializeApp() {
			this.platform.ready().then(() => {
			this.statusBar.styleDefault();
			this.splashScreen.hide();
			// this.nav.setRoot(LoginPage); 
		});

		this.auth.afAuth.authState
				.subscribe(
					user => {
                        if (user) {
                            this.rootParam = user.uid;
                            for (let data of user.providerData) {
                                //password or google.com
                                if (data.providerId === 'password') this.isCompany = false;
                                else if (data.providerId === 'google.com') this.isCompany = true;
                            }
							this.rootPage = HomePage;
						} else {
							this.rootPage = LoginPage;
						}
					},
					() => {
						this.rootPage = LoginPage;
					}
				);
	}

	logout() {
		this.menu.close();
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}

	openSetting()
	{
		this.nav.push(SettingsPage);
	}

	openProfile()
	{
		this.nav.push(ProfilePage);
	}

	openDashboard()
	{
		this.nav.setRoot(HomePage);  
	}

	openRequestPermission()
	{
		this.nav.push(PermissionRequestPage);
	}

	openMyPermission()
	{ 
		this.nav.push(PermissionHistoryPage);
	}

	openAttendanceHistory()
	{
        this.nav.push(PresenceHistoryPage, this.user);
	}

	openEmployees()
	{
		this.nav.push(AdminEmployeesListPage);
	}

	openEmployeesPermission()
	{
		this.nav.push(AdminPermissionListPage);
	}

	openEmployeesAdd()
	{
		this.nav.push(AdminEmployeesAddPage);
	}

	openNewsAdd()
	{
		this.nav.push(AdminNewsAddPage);
	}

	openNews()
	{
		this.nav.push(NewsHistoryPage);
	}

}

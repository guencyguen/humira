import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {IonicStorageModule} from '@ionic/storage';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {LoginPage} from '../pages/login/login';
import {RegisterPage} from '../pages/register/register';
import {NotificationsPage} from "../pages/notifications/notifications";
import {SettingsPage} from "../pages/settings/settings";
import {EditprofilePage} from "../pages/edit-profile/edit-profile";
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ProfilePage} from '../pages/profile/profile';
import {NewsService} from "../services/news-service";
import {AuthService} from '../services/auth.service';
import {DatePicker} from '@ionic-native/date-picker';
import {PresenceHistoryPage} from '../pages/presence-history/presence-history';

import {PermissionRequestPage} from '../pages/permission-request/permission-request';
import {PermissionHistoryPage} from '../pages/permission-history/permission-history';

import {Geolocation} from "@ionic-native/geolocation";
// firebase auth
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import {firebaseConfig} from '../config';
import {NewsHistoryPage} from '../pages/news-history/news-history';
import {NewsDetailPage} from '../pages/news-detail/news-detail';
import {UserService} from "../services/user-service";
import {AdminEmployeesAddPage} from '../pages/admin-employees-add/admin-employees-add';
import {AdminEmployeesEditPage} from '../pages/admin-employees-edit/admin-employees-edit';
import {AdminEmployeesListPage} from '../pages/admin-employees-list/admin-employees-list';
import {AdminNewsAddPage} from '../pages/admin-news-add/admin-news-add';
import {AdminNewsListPage} from '../pages/admin-news-list/admin-news-list';
import {AdminPermissionListPage} from '../pages/admin-permission-list/admin-permission-list';
import {AdminPermissionDetailPage} from '../pages/admin-permission-detail/admin-permission-detail';
import {AbsenService} from "../services/absen-service";
import {AgmCoreModule} from "@agm/core";
import {Camera} from "@ionic-native/camera";

@NgModule({
  declarations: [
    
    // Home
    MyApp,
    HomePage,

    // Login and Authentication
    ListPage,
    LoginPage,
    RegisterPage,

    // User Account
    ProfilePage,
    NotificationsPage,
    SettingsPage,
    EditprofilePage,

    // News
    NewsHistoryPage,
    NewsDetailPage,
    
    // HistoryAbsenPage,

    // Presence
    PresenceHistoryPage,

    // add employee
    AdminEmployeesAddPage,
    AdminEmployeesEditPage,
    AdminEmployeesListPage,
    AdminNewsAddPage,
    AdminNewsListPage,
    AdminPermissionListPage,
    AdminPermissionDetailPage,


    // Request Permission
    PermissionRequestPage,
    PermissionHistoryPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__ionic3_start_theme',
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    AngularFireModule.initializeApp(firebaseConfig.fire),
      AngularFireDatabaseModule,
      AgmCoreModule.forRoot({apiKey: "AIzaSyB8lYKH4BSbktK6UqIq8-yl8ptyYBinreM"})
  ],
  bootstrap: [IonicApp],
  entryComponents: [

    // Home
    MyApp,
    HomePage,

    // Login and Authentication
    ListPage,
    LoginPage,
    RegisterPage,

    // User Account
    ProfilePage,
    NotificationsPage,
    SettingsPage,
    EditprofilePage,

    // News
    NewsHistoryPage,
    NewsDetailPage,

    // HistoryAbsenPage,

    // Presence
    PresenceHistoryPage,

    // add employee
    AdminEmployeesAddPage,
    AdminEmployeesEditPage,
    AdminEmployeesListPage,
    AdminNewsAddPage,
    AdminNewsListPage,
    AdminPermissionListPage,
    AdminPermissionDetailPage,

    // Request Permission
    PermissionRequestPage,
    PermissionHistoryPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NewsService,
      UserService,
      AbsenService,
    DatePicker,
    AngularFireAuth,
      Geolocation,
      Camera,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

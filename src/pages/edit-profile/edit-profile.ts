import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-edit-profile',
	templateUrl: 'edit-profile.html',
})
export class EditprofilePage {

	// You can get this data from your API. This is a dumb data for being an example.
	public user_data = {
		profile_img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZPdaUSd54627psXPZRwQEVUnj-CMP0T5STo_39eu-wNRRzUwx',
		name_surname: 'Jeremias Rama',
		username: 'Jerry123',
		website: 'https://gitlab.com/jeremiasrama/humira/commits/master',
		description: 'Human Resource Director',
		email: 'jeremias.rama@gmail.com',
		phone: '',
		gender: 'male'
	};

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams, 
		public viewCtrl: ViewController,
		public loadingCtrl: LoadingController
		) {
	}

	updateProfile() {
		let loader = this.loadingCtrl.create({
		duration: 200
		});
		loader.present().then( () => this.navCtrl.pop() ); // Get back to profile page. You should do that after you got data from API
	}

	dismiss() {
	this.viewCtrl.dismiss();
	}

}

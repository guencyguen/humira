import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BaseAPI } from '../../services/BaseAPI';

/**
 * Generated class for the AdminEmployeesAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-admin-employees-add',
  templateUrl: 'admin-employees-add.html',
})
export class AdminEmployeesAddPage {
  addEmployeeForm: FormGroup;
  myDate: String = new Date().toISOString();

  constructor(
    public navCtrl: NavController, 
    public writedb: AngularFireDatabase,
    public navParams: NavParams,
    FB: FormBuilder
    ) {
      this.addEmployeeForm = FB.group({
        nama: ['', Validators.compose([Validators.required])],
        date_birth: ['', Validators.compose([Validators.required])],
        date_join: ['', Validators.compose([Validators.required])],
        id_usergroup: ['', Validators.compose([Validators.required])],
        img: ['', Validators.compose([Validators.required])],
        phonenum: ['', Validators.compose([Validators.required])],
        id_company: ['', Validators.compose([Validators.required])],
        email: ['', Validators.compose([Validators.required])],
        password: ['', Validators.compose([Validators.required])],
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminEmployeesAddPage');
  }

  btnAddClicked(){
    let site = this.addEmployeeForm.value;
    site.active = '0';
    BaseAPI.getFromDB('user').then(data => {
        console.log(data.val().length);
        BaseAPI.setToDB('user/' + data.val().length, site).then(ok => {
            console.log(ok);
        });
        // this.writedb.list("/myItems/").push(this.myInputtest);
    });
  }

}

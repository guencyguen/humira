import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {LoginPage} from "../login/login";
import {HomePage} from "../home/home";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  signupError: string;
	form: FormGroup;
  constructor(
      fb: FormBuilder,
      private nav: NavController,
      private auth: AuthService
    ) {
      this.form = fb.group({
        email: ['', Validators.compose([Validators.required, Validators.email])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
      });
  }

  // register and go to home page
  // signup() {
	// 	let data = this.form.value;
	// 	let credentials = {
	// 		email: data.email,
	// 		password: data.password
	// 	};
	// 	this.auth.signUp(credentials).then(
	// 		() => this.nav.setRoot(HomePage),
	// 		error => this.signupError = error.message
	// 	);
  // }

  loginWithGoogle() {
      this.auth.signInWithGoogle().then(
          (result) => {
              result.user.getIdToken().then(data => console.log(data));
              this.nav.setRoot(HomePage);
          },
        error => console.log(error.message)
      );
    }

  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  }
}

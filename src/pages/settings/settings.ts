import {Component} from "@angular/core";
import {NavController, MenuController} from "ionic-angular";
import {LoginPage} from "../login/login";
import { AuthService } from "../../services/auth.service";


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  menu: MenuController;

  constructor(
      public nav: NavController,
      menu: MenuController,
		  private auth: AuthService
    ) {
      this.menu = menu;
  }

  // logout
  logout() {
		this.menu.close();
		this.auth.signOut();
		this.nav.setRoot(LoginPage);
	}
}

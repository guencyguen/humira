import {Component} from '@angular/core';
import {NavController, NavParams, PopoverController} from 'ionic-angular';
import {NotificationsPage} from "../notifications/notifications";
import {SettingsPage} from "../settings/settings";
import {AuthService} from "../../services/auth.service";
import {BaseAPI} from "../../services/BaseAPI";
import * as firebase from "firebase";
import {UserService} from "../../services/user-service";
import {NewsService} from "../../services/news-service";

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})

export class HomePage {

    private userdata: any;
    private absendata: any[];
    private btn_txt: string;
    private masukterakhir: number;

    constructor(public authService: AuthService, public navCtrl: NavController,
                public userService: UserService, public newsService: NewsService,
                public popoverCtrl: PopoverController, public navParam: NavParams) {
        this.authService.afAuth.authState.subscribe(data => {
            if (data) {
                this.userdata = data;
                firebase.database().ref('/Humira/absen/' + data.uid).on('value', absendata => {
                    this.absendata = absendata.val();
                    if (this.absendata != undefined) {
                        console.log('logged in');
                        this.masukterakhir = this.absendata[this.absendata.length - 1]['masuk'];
                        this.btn_txt = this.absendata[this.absendata.length - 1]['keluar'] != undefined ? 'Check In' : 'Check Out';
                    }
                    else {
                        // this.masukterakhir = "never";
                        this.absendata = [];
                        this.btn_txt = 'Check In';
                    }
                });
            }
        });
  }

	goToAccount()
	{
		this.navCtrl.push(SettingsPage);
	}

	presentNotifications(myEvent)  
	{
		console.log(myEvent);
		let popover = this.popoverCtrl.create(NotificationsPage);
		popover.present({
			ev: myEvent
		});
	}

	zipped: boolean = true;
	toggleZipped(): void {
  		this.zipped = !this.zipped;
	}

	check() {
        if (this.btn_txt == 'Check In') {
            this.btn_txt = null;
            BaseAPI.setToDB('absen/' + this.userdata.uid + '/' + this.absendata.length, {
                masuk: firebase.database.ServerValue.TIMESTAMP,
                status: 'valid'
            }).then(ok => 'ok'
            );
        }
		else {
            this.btn_txt = null;
            BaseAPI.setToDB('absen/' + this.userdata.uid + '/' + (this.absendata.length - 1), {keluar: firebase.database.ServerValue.TIMESTAMP}).then(ok => 'ok'
            );
		}
	}

    getNews() {
        return this.newsService.getAll();
    }

    getAuthor(id) {
        if (this.userService.getAll().length > 0)
            return this.userService.getItem(parseInt(id));
    }
}

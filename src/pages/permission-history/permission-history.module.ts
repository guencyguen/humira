import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PermissionHistoryPage } from './permission-history';

@NgModule({
  declarations: [
    PermissionHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PermissionHistoryPage),
  ],
})
export class PermissionHistoryPageModule {}

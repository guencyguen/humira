import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PermissionRequestPage } from '../permission-request/permission-request';

/**
 * Generated class for the PermissionHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-permission-history',
  templateUrl: 'permission-history.html',
})
export class PermissionHistoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PermissionHistoryPage');
  }

  requestPermission()
  {
    this.navCtrl.push(PermissionRequestPage);
  }

}

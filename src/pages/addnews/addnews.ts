import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {HomePage} from "../home/home";

/**
 * Generated class for the AddnewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-addnews',
  templateUrl: 'addnews.html',
})
export class AddnewsPage {
  
  constructor(public nav: NavController, public navParams: NavParams) {
  }

  goSubmitAddnews() {
    this.nav.setRoot(HomePage);
  }
  goCancelAddnews() {
    this.nav.setRoot(HomePage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddnewsPage');
  }

}

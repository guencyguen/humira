import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {NewsService} from "../../services/news-service";
import {NewsDetailPage} from "../news-detail/news-detail";
import {UserService} from "../../services/user-service";

@Component({
  selector: 'page-news-history',
  templateUrl: 'news-history.html'
})
export class NewsHistoryPage {
  // list of news
    public news: Array<Object>;

    constructor(public nav: NavController, public userService: UserService, public newsService: NewsService) {
    // set sample data
  }

    ngOnInit() {
    }

  // view trip detail
  viewDetail(id) {
    this.nav.push(NewsDetailPage, {id: id});
  }

    getNews() {
        return this.newsService.getAll();
    }

    getAuthor(id) {
        if (this.userService.getAll().length > 0)
            return this.userService.getItem(parseInt(id));
    }
}

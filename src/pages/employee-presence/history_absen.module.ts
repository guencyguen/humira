import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryAbsenPage } from './history_absen';

@NgModule({
  declarations: [
    HistoryAbsenPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryAbsenPage),
  ],
  exports: [
    HistoryAbsenPage
  ]
})
export class NotificationsModule {}

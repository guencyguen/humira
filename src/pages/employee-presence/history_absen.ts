import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-historyabsen',
  templateUrl: 'history_absen.html',
})
export class HistoryAbsenPage {

  public page:string = 'you';

  public notifications = [
    {
      
    }
  ];
  public notifications_you = [
    {
      id: 1,
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Notifications');
  }

}

import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {NewsService} from "../../services/news-service";
import {UserService} from "../../services/user-service";

// import {CheckoutTripPage} from "../checkout-trip/checkout-trip";

@Component({
  selector: 'page-news-detail',
  templateUrl: 'news-detail.html'
})
export class NewsDetailPage {
  
  public new: any;

    constructor(
      public nav: NavController, 
      public userService: UserService,
      public newsService: NewsService,
      public navParam: NavParams
      ) {
    // set sample data
  }

    ngOnInit() {
        this.new = this.newsService.getItem(parseInt(this.navParam.get('id')));
    }

    getAuthor(id) {
      if (this.userService.getAll().length > 0)
          return this.userService.getItem(parseInt(id));
  }

 
}

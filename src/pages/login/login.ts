import {Component} from "@angular/core";
import {AlertController, MenuController, NavController, ToastController} from "ionic-angular";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loginForm: FormGroup;
	loginError: string;

  constructor(
      public nav: NavController, 
      public forgotCtrl: AlertController, 
      public menu: MenuController, 
      public toastCtrl: ToastController,
      private auth: AuthService,
      fb: FormBuilder
    ) {
    this.menu.swipeEnable(false);
    this.loginForm = fb.group({
			email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
		});
  }

  // go to register page
  register() {
    this.nav.setRoot(RegisterPage);
  }

  // login and go to home page
  login() {
      this.loginError = null;
      let data = this.loginForm.value;

      if (!data.email) {
        return;
      }

      let credentials = {
        email: data.email,
        password: data.password
      };
      this.auth.signInWithEmail(credentials)
          .then(
              (result) => {
                  result.user.getIdToken().then(data => console.log(data));
                  this.nav.setRoot(HomePage, result.user);
              },
              error => this.loginError = error.message
          );
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}

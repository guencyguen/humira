import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {AngularFireDatabase} from 'angularfire2/database';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BaseAPI} from "../../services/BaseAPI";

/**
 * Generated class for the AdminNewsAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-admin-news-add',
  templateUrl: 'admin-news-add.html',
})
export class AdminNewsAddPage {

    // arrData = []
    // myInputtest
    addNewsForm: FormGroup;

    // site = [{
    //   content: 'grokonez.com',
    //   date: '2018-12-07 07:15:00',
    //   id_user: '0',
    //   image: 'assets/img/trip/thumb/trip_2.jpg',
    //   priority: '0',
    //   title: 'test aja'
    // }];

  constructor(
    public navCtrl: NavController, 
    public writedb: AngularFireDatabase,
    public navParams: NavParams,
    FB: FormBuilder
    ) {
      // this.writedb.list("/myItems/").subscribe(_data => {
      //   this.arrData = _data;
        // this.writedb.list('/Humira/news').push(this.site);
      //   console.log(this.arrData);
      // });


      this.addNewsForm = FB.group({
        title: ['', Validators.compose([Validators.required])],
        date: ['', Validators.compose([Validators.required])],
        priority: ['', Validators.compose([Validators.required])],
        content: ['', Validators.compose([Validators.required])],
        image: ['', Validators.compose([Validators.required])]
      });
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad AdminNewsAddPage');
  // }

  btnAddClicked(){
      let site = this.addNewsForm.value;
      site.id_user = '0';
      BaseAPI.getFromDB('news').then(data => {
          console.log(data.val().length);
          BaseAPI.setToDB('news/' + data.val().length, site).then(ok => {
              console.log(ok);
          });
          // this.writedb.list("/myItems/").push(this.myInputtest);
      });
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PresenceHistoryPage } from './presence-history';

@NgModule({
  declarations: [
    PresenceHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PresenceHistoryPage),
  ],
})
export class PresenceHistoryPageModule {}

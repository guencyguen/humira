import {Component} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {AbsenService} from "../../services/absen-service";

import * as moment from 'moment';

/**
 * Generated class for the PresenceHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-presence-history',
  templateUrl: 'presence-history.html',
})
export class PresenceHistoryPage {

    selectedDay = new Date();

    calendar = {
        mode: 'month',
        currentDate: this.selectedDay
    };

    public absenService: AbsenService;
    eventSource: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, privatemodalCtrl: ModalController,
                private alertCtrl: AlertController) {
        this.absenService = new AbsenService(this.navParams.data.uid);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad PresenceHistoryPage');
    }

    getAbsenData() {
        return this.absenService.getAll();
    }

    onTimeSelected(ev) {
        this.selectedDay = ev.selectedTime;
    }

    onEventSelected(event) {
        let start = moment(event.startTime).format('LLLL');
        let end = moment(event.endTime).format('LLLL');

        let alert = this.alertCtrl.create({
            //  title: '' +event.title,
            // subTitle: 'from : ' + start + '<br>To: ' + end,
            buttons: ['Ok']
        });
        alert.present();

    }
}

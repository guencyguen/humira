import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PulangCepatPage } from './pulang-cepat';

@NgModule({
  declarations: [
    PulangCepatPage,
  ],
  imports: [
    IonicPageModule.forChild(PulangCepatPage),
  ],
})
export class PulangCepatPageModule {}

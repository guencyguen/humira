import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PermissionRequestPage } from './permission-request';

@NgModule({
  declarations: [
    PermissionRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(PermissionRequestPage),
  ],
})
export class PermissionRequestPageModule {}

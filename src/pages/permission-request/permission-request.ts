import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
// import { DatePicker } from '@ionic-native/date-picker';
import {AngularFireDatabase} from 'angularfire2/database';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BaseAPI} from '../../services/BaseAPI';
import {DomSanitizer} from "@angular/platform-browser";
// import {AndroidPermissions} from "@ionic-native/android-permissions";
// import {Camera, CameraOptions} from "@ionic-native/camera";
// import {File} from "@ionic-native/file";

@IonicPage()
@Component({
  selector: 'page-permission-request',
  templateUrl: 'permission-request.html',
})

export class PermissionRequestPage {
	addPRForm: FormGroup;
    imageUrl: string;
	constructor(
        public navCtrl: NavController,
        public writedb: AngularFireDatabase,
        public navParams: NavParams,
        FB: FormBuilder,
        // public camera: Camera, public androPerm: AndroidPermissions,
        public toastCtrl: ToastController, public domSanitizer: DomSanitizer
        // public file: File
		) {
			this.addPRForm = FB.group({
				nama: ['', Validators.compose([Validators.required])],
				alasan: ['', Validators.compose([Validators.required])],
				tanggal_mulai: ['', Validators.compose([Validators.required])],
				tanggal_selesai: ['', Validators.compose([Validators.required])],
				img_pendukung: ['', Validators.compose([Validators.required])],
				img_pendukung2: ['', Validators.compose([Validators.required])],
				id_company: ['', Validators.compose([Validators.required])]
			  });
	}

	btnAddClicked()
	{
		let site = this.addPRForm.value;
		BaseAPI.getFromDB('izin').then(data => {
			console.log(data.val().length);
			BaseAPI.setToDB('izin/' + data.val().length, site).then(ok => {
				console.log(ok);
			});
			// this.writedb.list("/myItems/").push(this.myInputtest);
		});
	}

    // onTakePhoto() {
    //     const options: CameraOptions = {
    //         destinationType: this.camera.DestinationType.FILE_URI,
    //         encodingType: this.camera.EncodingType.JPEG,
    //         correctOrientation: true,
    //         targetHeight: 512,
    //         targetWidth: 512,
    //         quality: 100,
    //         mediaType: this.camera.MediaType.PICTURE
    //     };
    //     this.androPerm.checkPermission(this.androPerm.PERMISSION.CAMERA)
    //         .then(success => this.camera.getPicture(options)
    //             .then(img => {
    //                 let filename = img.substring(img.lastIndexOf('/') + 1);
    //                 let path = img.substring(0, img.lastIndexOf('/') + 1);
    //                 this.file.readAsDataURL(path, filename).then(res => this.imageUrl = res);
    //                 this.toastCtrl.create({
    //                     message: "Saved to " + this.imageUrl,
    //                     duration: 5000,
    //                 }).present();
    //             }), err => {
    //             this.androPerm.requestPermission(this.androPerm.PERMISSION.CAMERA)
    //                 .then(data => {
    //                     this.toastCtrl.create({
    //                         message: "Request success\nPlease try click the button again.",
    //                         duration: 5000,
    //                     }).present()
    //                 }, err => {
    //                     this.toastCtrl.create({
    //                         message: "Request failed!\n" + err,
    //                         duration: 5000,
    //                     }).present()
    //                 });
    //         })
    // }

}
 